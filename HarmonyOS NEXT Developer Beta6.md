# HarmonyOS NEXT Developer Beta6



大家好，昨天在看官网的时候，发现Beta6已经发布，在细读变更文档后发现，本次主要聚焦在OS平台能力的增强，新增部分C API能力，如UDMF、组件拖拽、密码算法等；RDB新增支持sendable的能力等。针对IDE，并没有做更新。[欢迎大家下载体验。](https://developer.huawei.com/consumer/cn/download/)

![image-20240831095150458](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240831095150458.png)

## HarmonyOS NEXT Developer Beta6

| **软件包**    | **发布类型**   | **版本号**                         | Build Version                                               | **发布时间** |
| ------------- | -------------- | ---------------------------------- | ----------------------------------------------------------- | ------------ |
| 系统          | Developer Beta | HarmonyOS NEXT Developer Beta6     | NEXT.0.0.61                                                 | 2024/08/28   |
| DevEco Studio | Developer Beta | DevEco Studio NEXT Developer Beta6 | 5.0.3.706                                                   | 2024/08/28   |
| SDK           | Developer Beta | HarmonyOS NEXT Developer Beta6 SDK | 基于OpenHarmony SDK Ohos_sdk_public 5.0.0.61 (API 12 Beta6) | 2024/08/28   |

## Ability Kit

新增支持传参和异步ArkTS API调用的子进程启动方法。

## Account Kit

新增获取手机号一致性状态，实现用户更安全、更便捷的静默登录。

## ArkData

- 支持通过C API调用UDMF提供的统一数据管理和拖拽通道的能力。
- 提供新增模块@ohos.data.sendableRelationalStore，使能关系型数据库（RDB）支持sendable能力。
- 支持在关系型数据库（RDB）中引入支持fts（Full-Text Search，即全文搜索引擎）等功能的动态库的能力。

## ArkGraphics 2D

绘制模块在原有C API能力的基础上，补齐ArkTS API能力。

## ArkUI

- 提供通用拖拽能力的C API。
- Grid组件支持选取一行中最高的GridItem的高度定义为行高。
- Tabs组件支持通过无感监听上报TabContent页面的切换事件。
- 屏幕属性新增接口用于获取折叠设备的显示模式以及对应的物理屏幕分辨率信息对象；新增接口用于获取2in1设备屏幕的可用区域
- Image组件新增ImageContent传参类型，允许当设置的图片源为空时，Image组件显示为空。

## Crypto Architecture Kit

密钥生成和转换、加解密、签名验签、消息摘要等能力新增支持通过C API调用，同步提供开发指南。

## IAP Kit

支持订阅场景下使用优惠促销。

## Service Collaboration Kit

支持在跨设备互通场景下，自定义接收图片的最大张数。

## Pen Kit

新增全局取色功能。